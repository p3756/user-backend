import { isEmpty, MinLength, matches } from 'class-validator';
import { minLength } from 'class-validator/types/decorator/decorators';
export class CreateUserDto {
  @isEmpty
  @minLength(5)
  login: string;

  @minLength(5)
  @isEmpty
  name: string;

  @minLength(5)
  @isEmpty
  @matches(/^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/)
  password: string;
}
